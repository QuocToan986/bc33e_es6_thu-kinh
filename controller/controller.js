export let renderGlass = (list) => {
  let content = "";
  list.forEach((element) => {
    content += `
   <img src= ${element.src} alt="" onclick = "getGlass('${element.id}')" class = "col-4"/>
    `;
  });
  document.getElementById("vglassesList").innerHTML = content;
};

export let findID = (id, arr) => {
  let glassNumber = "";
  for (let index = 0; index < arr.length; index++) {
    glassNumber = arr[index];
    if (glassNumber.id == id) {
      return index;
    }
  }
  return -1;
};

export let renderIntroGlass = (list) => {
  let introduce = "";
  introduce += `
    <div>
    <p class = "mb-1">${list.name} - ${list.brand} (${list.color})</p>
    <span class = "btn btn-danger">$${list.price}</span>
    <span class = "text-success pl-2"> <i>Stocking</i></span>
    <p class = "mt-4 mb-0">${list.description}</p>
    </div>
    `;

  document.querySelector(".vglasses__info").style.display = "block";
  document.getElementById("glassesInfo").innerHTML = introduce;
};

export let checkButton = (input, img) => {
  if (input) {
    img.style.display = "block";
  } else {
    img.style.display = "none";
  }
};
